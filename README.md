The project is basically about a link layer network protocol, which includes some improvement with respect to Go-Back-N protocol. The implementation is done using a performance evaluation process algebra modelling language.

The contributors of the project are Hootan Rashtian and Alireza Zakeri-Hosseinabadi.